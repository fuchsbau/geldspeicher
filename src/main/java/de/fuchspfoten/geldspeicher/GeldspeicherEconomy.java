package de.fuchspfoten.geldspeicher;

import de.fuchspfoten.fuchslib.data.PlayerData;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;
import org.bukkit.OfflinePlayer;

import java.util.Collections;
import java.util.List;

/**
 * Class that hooks the economy into Vault.
 */
public class GeldspeicherEconomy implements Economy {

    @Override
    public boolean isEnabled() {
        return GeldspeicherPlugin.getSelf().isEnabled();
    }

    @Override
    public String getName() {
        return "Geldspeicher";
    }

    @Override
    public boolean hasBankSupport() {
        return false;
    }

    @Override
    public int fractionalDigits() {
        return 2;
    }

    /**
     * Internal proxy method for getBalance.
     *
     * @param data The player data.
     * @return The balance.
     */
    private static double getBalance(final PlayerData data) {
        return data.getStorage().getDouble("economy.balanceHand", 0.0);
    }

    @Override
    public String currencyNamePlural() {
        return GeldspeicherPlugin.getSelf().getCurrencyPlural();
    }

    @Override
    public String currencyNameSingular() {
        return GeldspeicherPlugin.getSelf().getCurrencySingular();
    }

    @Override
    public boolean hasAccount(final String playerName) {
        return true;
    }

    @Override
    public boolean hasAccount(final OfflinePlayer player) {
        return true;
    }

    @Override
    public boolean hasAccount(final String playerName, final String worldName) {
        return hasAccount(playerName);
    }

    @Override
    public boolean hasAccount(final OfflinePlayer player, final String worldName) {
        return hasAccount(player);
    }

    @Override
    public double getBalance(final String playerName) {
        return getBalance(new PlayerData(playerName));
    }

    @Override
    public double getBalance(final OfflinePlayer player) {
        return getBalance(new PlayerData(player));
    }

    /**
     * Internal proxy method for withdrawPlayer.
     *
     * @param data The player data.
     * @param amount The amount to withdraw.
     * @return The response for this action.
     */
    private static EconomyResponse withdrawPlayer(final PlayerData data, final double amount) {
        if (amount < 0 || Double.isNaN(amount) || Double.isInfinite(amount)) {
            throw new IllegalArgumentException("Trying to withdraw illegal amounts: " + amount);
        }

        final double balance = getBalance(data);
        if (balance < amount) {
            data.getStorage().set("economy.balanceHand", 0);
            return new EconomyResponse(balance, 0, ResponseType.SUCCESS, null);
        }
        data.getStorage().set("economy.balanceHand", balance - amount);
        return new EconomyResponse(amount, balance - amount, ResponseType.SUCCESS, null);
    }

    @Override
    public double getBalance(final String playerName, final String world) {
        return getBalance(playerName);
    }

    @Override
    public double getBalance(final OfflinePlayer player, final String world) {
        return getBalance(player);
    }

    @Override
    public boolean has(final String playerName, final double amount) {
        return getBalance(playerName) >= amount;
    }

    @Override
    public boolean has(final OfflinePlayer player, final double amount) {
        return getBalance(player) >= amount;
    }

    @Override
    public boolean has(final String playerName, final String worldName, final double amount) {
        return has(playerName, amount);
    }

    @Override
    public boolean has(final OfflinePlayer player, final String worldName, final double amount) {
        return has(player, amount);
    }

    @Override
    public EconomyResponse withdrawPlayer(final String playerName, final double amount) {
        return withdrawPlayer(new PlayerData(playerName), amount);
    }

    @Override
    public EconomyResponse withdrawPlayer(final OfflinePlayer player, final double amount) {
        return withdrawPlayer(new PlayerData(player), amount);
    }

    /**
     * Internal proxy method for depositPlayer.
     *
     * @param data The player data.
     * @param amount The amount to deposit.
     * @return The response for this action.
     */
    private static EconomyResponse depositPlayer(final PlayerData data, final double amount) {
        if (amount < 0 || Double.isNaN(amount) || Double.isInfinite(amount)) {
            throw new IllegalArgumentException("Trying to deposit illegal amounts: " + amount);
        }

        final double balance = getBalance(data);
        final double limit = GeldspeicherPlugin.getSelf().getMoneyLimit();
        if (balance + amount > limit) {
            data.getStorage().set("economy.balanceHand", limit);
            return new EconomyResponse(limit - balance, limit, ResponseType.SUCCESS,
                    null);
        }
        data.getStorage().set("economy.balanceHand", balance + amount);
        return new EconomyResponse(amount, balance + amount, ResponseType.SUCCESS, null);
    }

    @Override
    public EconomyResponse withdrawPlayer(final String playerName, final String worldName, final double amount) {
        return withdrawPlayer(playerName, amount);
    }

    @Override
    public EconomyResponse withdrawPlayer(final OfflinePlayer player, final String worldName, final double amount) {
        return withdrawPlayer(player, amount);
    }

    @Override
    public String format(final double amount) {
        if (Math.abs(amount - 1) < 0.01) {
            return String.format(GeldspeicherPlugin.getSelf().getAmountFormat(), amount, currencyNameSingular());
        }
        return String.format(GeldspeicherPlugin.getSelf().getAmountFormat(), amount, currencyNamePlural());
    }

    @Override
    public EconomyResponse depositPlayer(final String playerName, final double amount) {
        return depositPlayer(new PlayerData(playerName), amount);
    }

    @Override
    public EconomyResponse depositPlayer(final OfflinePlayer player, final double amount) {
        return depositPlayer(new PlayerData(player), amount);
    }

    @Override
    public EconomyResponse depositPlayer(final String playerName, final String worldName, final double amount) {
        return depositPlayer(playerName, amount);
    }

    @Override
    public EconomyResponse depositPlayer(final OfflinePlayer player, final String worldName, final double amount) {
        return depositPlayer(player, amount);
    }

    @Override
    public EconomyResponse createBank(final String name, final String player) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse createBank(final String name, final OfflinePlayer player) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse deleteBank(final String name) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse bankBalance(final String name) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse bankHas(final String name, final double amount) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse bankWithdraw(final String name, final double amount) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse bankDeposit(final String name, final double amount) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse isBankOwner(final String name, final String playerName) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse isBankOwner(final String name, final OfflinePlayer player) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse isBankMember(final String name, final String playerName) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public EconomyResponse isBankMember(final String name, final OfflinePlayer player) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED,
                null);
    }

    @Override
    public List<String> getBanks() {
        return Collections.emptyList();
    }

    @Override
    public boolean createPlayerAccount(final String name) {
        return true;
    }

    @Override
    public boolean createPlayerAccount(final OfflinePlayer player) {
        return true;
    }

    @Override
    public boolean createPlayerAccount(final String playerName, final String worldName) {
        return true;
    }

    @Override
    public boolean createPlayerAccount(final OfflinePlayer player, final String worldName) {
        return true;
    }
}
