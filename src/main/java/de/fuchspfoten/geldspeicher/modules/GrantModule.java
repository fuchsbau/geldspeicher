package de.fuchspfoten.geldspeicher.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import de.fuchspfoten.geldspeicher.GeldspeicherPlugin;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * /grant &lt;player&gt; &lt;amount&gt; transfers money from the server account.
 */
public class GrantModule implements CommandExecutor {

    /**
     * The parser for command arguments for /pay.
     */
    private final ArgumentParser argumentParserPay;

    /**
     * Constructor.
     */
    public GrantModule() {
        Messenger.register("geldspeicher.grantSend");
        Messenger.register("geldspeicher.grantReceive");
        Messenger.register("geldspeicher.noNumber");

        argumentParserPay = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("geldspeicher.grant")) {
            sender.sendMessage("Missing permission: geldspeicher.grant");
            return true;
        }

        // Syntax check.
        args = argumentParserPay.parse(args);
        if (args.length != 2 || argumentParserPay.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /grant <target> <amount>");
            argumentParserPay.showHelp(sender);
            return true;
        }

        // Retrieve the target.
        final OfflinePlayer target = Bukkit.getOfflinePlayer(UUIDLookup.lookup(args[0]));

        // Retrieve the amount.
        final double amount;
        try {
            amount = Double.parseDouble(args[1]);
            if (Double.isInfinite(amount) || Double.isNaN(amount)) {
                Messenger.send(sender, "geldspeicher.noNumber");
                return true;
            }
        } catch (final NumberFormatException ex) {
            Messenger.send(sender, "geldspeicher.noNumber");
            return true;
        }

        // Perform the transaction.
        final Economy eco = GeldspeicherPlugin.getSelf().getEconomy();
        final EconomyResponse response;
        if (amount < 0) {
            response = eco.withdrawPlayer(target, -amount);
        } else {
            response = eco.depositPlayer(target, amount);
        }

        // Log the transaction.
        final PlayerData pd = new PlayerData(target);
        pd.modifyDouble("economy.logGrant", response.amount, 0.0);

        // Notify both parties.
        Messenger.send(sender, "geldspeicher.grantSend", eco.format(amount), target.getName());
        if (target.isOnline()) {
            Messenger.send(target.getPlayer(), "geldspeicher.grantReceive", eco.format(amount));
        }
        return true;
    }
}
