package de.fuchspfoten.geldspeicher.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import de.fuchspfoten.geldspeicher.GeldspeicherPlugin;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * /money [player] shows the balance of a player.
 */
public class MoneyModule implements CommandExecutor {

    /**
     * The parser for command arguments for /money.
     */
    private final ArgumentParser argumentParserMoney;

    /**
     * Constructor.
     */
    public MoneyModule() {
        Messenger.register("geldspeicher.moneySelf");
        Messenger.register("geldspeicher.moneyOther");

        argumentParserMoney = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Syntax check.
        args = argumentParserMoney.parse(args);
        if (args.length > 1 || argumentParserMoney.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /money [player]");
            argumentParserMoney.showHelp(sender);
            return true;
        }

        if (args.length == 1) {
            // Permission check.
            if (!sender.hasPermission("geldspeicher.inspect")) {
                sender.sendMessage("Missing permission: geldspeicher.inspect");
                return true;
            }

            final UUID target = UUIDLookup.lookup(args[0]);
            final String balance = GeldspeicherPlugin.getSelf().getEconomy().format(
                    GeldspeicherPlugin.getSelf().getEconomy().getBalance(Bukkit.getOfflinePlayer(target)));
            Messenger.send(sender, "geldspeicher.moneyOther", balance, args[0]);
        } else if (sender instanceof Player) {
            final String balance = GeldspeicherPlugin.getSelf().getEconomy().format(
                    GeldspeicherPlugin.getSelf().getEconomy().getBalance((OfflinePlayer) sender));
            Messenger.send(sender, "geldspeicher.moneySelf", balance);
        } else {
            sender.sendMessage("Invalid usage.");
        }
        return true;
    }
}
