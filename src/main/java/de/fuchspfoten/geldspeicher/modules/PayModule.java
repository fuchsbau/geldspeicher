package de.fuchspfoten.geldspeicher.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.geldspeicher.GeldspeicherPlugin;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /pay &lt;player&gt; &lt;amount&gt; transfers money to a player.
 */
public class PayModule implements CommandExecutor {

    /**
     * The parser for command arguments for /pay.
     */
    private final ArgumentParser argumentParserPay;

    /**
     * Constructor.
     */
    public PayModule() {
        Messenger.register("geldspeicher.paySend");
        Messenger.register("geldspeicher.payReceive");
        Messenger.register("geldspeicher.payNotOnline");
        Messenger.register("geldspeicher.noNumber");
        Messenger.register("geldspeicher.payNoFunds");

        argumentParserPay = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Syntax check.
        args = argumentParserPay.parse(args);
        if (args.length != 2 || argumentParserPay.hasArgument('h') || !(sender instanceof Player)) {
            Messenger.send(sender, "commandHelp", "Syntax: /pay <target> <amount>");
            argumentParserPay.showHelp(sender);
            return true;
        }
        final Player origin = (Player) sender;

        // Retrieve the target.
        final Player target = Bukkit.getPlayer(args[0]);
        if (target == null || !target.isOnline() || !origin.canSee(target)) {
            Messenger.send(sender, "geldspeicher.payNotOnline", args[0]);
            return true;
        }

        // Retrieve the amount.
        final double amount;
        try {
            amount = Double.parseDouble(args[1]);
            if (Double.isInfinite(amount) || Double.isNaN(amount) || amount < 0.01) {
                Messenger.send(sender, "geldspeicher.noNumber");
                return true;
            }
        } catch (final NumberFormatException ex) {
            Messenger.send(sender, "geldspeicher.noNumber");
            return true;
        }

        // Check the funds of the sender.
        final Economy eco = GeldspeicherPlugin.getSelf().getEconomy();
        if (!eco.has(origin, amount)) {
            Messenger.send(sender, "geldspeicher.payNoFunds");
            return true;
        }

        // Perform the transaction.
        eco.withdrawPlayer(origin, amount);
        eco.depositPlayer(target, amount);

        // Log the transaction.
        final PlayerData senderData = new PlayerData(origin);
        senderData.modifyDouble("economy.logSent." + target.getUniqueId(), amount, 0.0);
        final PlayerData receiverData = new PlayerData(target);
        receiverData.modifyDouble("economy.logReceived." + origin.getUniqueId(), amount, 0.0);

        // Notify both parties.
        Messenger.send(origin, "geldspeicher.paySend", eco.format(amount), target.getName());
        Messenger.send(target, "geldspeicher.payReceive", eco.format(amount), origin.getName());
        return true;
    }
}
