package de.fuchspfoten.geldspeicher;

import de.fuchspfoten.geldspeicher.modules.GrantModule;
import de.fuchspfoten.geldspeicher.modules.MoneyModule;
import de.fuchspfoten.geldspeicher.modules.PayModule;
import lombok.Getter;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main plugin class.
 */
public class GeldspeicherPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static GeldspeicherPlugin self;

    /**
     * Format string for money amounts.
     */
    private @Getter String amountFormat;

    /**
     * Name of the currency, singular.
     */
    private @Getter String currencySingular;

    /**
     * Name of the currency, plural.
     */
    private @Getter String currencyPlural;

    /**
     * The money limit.
     */
    private @Getter double moneyLimit;

    /**
     * The economy provider.
     */
    private @Getter Economy economy;

    @Override
    public void onEnable() {
        self = this;

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Read the amount format.
        amountFormat = getConfig().getString("amountFormat");

        // Read the currency names.
        currencySingular = getConfig().getString("currencySingular");
        currencyPlural = getConfig().getString("currencyPlural");

        // Read the money limit.
        moneyLimit = getConfig().getDouble("moneyLimit");

        // Hook into vault.
        economy = new GeldspeicherEconomy();
        Bukkit.getServicesManager().register(Economy.class, economy, this, ServicePriority.Normal);

        // Register modules.
        getCommand("grant").setExecutor(new GrantModule());
        getCommand("money").setExecutor(new MoneyModule());
        getCommand("pay").setExecutor(new PayModule());
    }

    @Override
    public void onDisable() {
        Bukkit.getServicesManager().unregister(Economy.class, economy);
    }
}
